/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.restservices;

import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.dao.SempicUserFacade;
import fr.uga.miashs.sempic.entities.SempicUser;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Pierre
 */
@Stateless
public class SempicUserService {
    @Inject
    private SempicUserFacade userDAO;
    
    @Path("/users")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<SempicUser> listAll(){
        return userDAO.findAll();
    }
    
    @Path("/users/{id}")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public SempicUser get(@PathParam("id") long id){
        return userDAO.readEager(id);
    }
    
    @Path("/users/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_XML)
    public SempicUser delete(@PathParam("id") long id){
        //TODO : changer méthode pour supprimer;
        return userDAO.readEager(id);
    }
    
    @Path("/users")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_XML)
    public Response create(SempicUser u, @Context UriInfo info) throws SempicModelException{
        long id = userDAO.create(u);
        URI location = info.getBaseUriBuilder().path(String.valueOf(id)).build();
        return Response.created(location).entity(u).build();
    }
}

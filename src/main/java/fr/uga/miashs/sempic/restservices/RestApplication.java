/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.restservices;

import fr.uga.miashs.sempic.ApplicationConfig;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

/**
 *
 * @author Pierre
 */
@ApplicationPath(ApplicationConfig.WEB_API)
public class RestApplication extends Application{
    
    /**
     *
     * @return
     */
    @Override
    public Set<Class<?>> getClasses(){
        Set res = new HashSet();
        String[] features = {
            "org.glassfish.jersey.moxy.json.MoxyJsonFeature"/*,
            "ICI ON AJOUTE D'AUTRES FEATURES"*/
        };
        for(String f : features){
            try{
                Class cls = Class.forName(f);
                res.add(cls);
            }catch (ClassNotFoundException ex){
                Logger.getLogger(RestApplication.class.getName()).log(Level.WARNING, "{0} not available", f);
            }
        }
        res.add(MOXyJsonProvider.class);   
        res.add(SempicUserService.class);
        /*  
            ICI ON PEUT AJOUTER ENCORE DES TRUCS PRESENTS DANS LE CLASSPATH PAR EXEMPLE 
            res.add(UserService.class);
        */
        return res;
    }
}

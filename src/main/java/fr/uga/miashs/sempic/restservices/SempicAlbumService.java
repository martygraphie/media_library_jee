/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.restservices;

import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.dao.AlbumFacade;
import fr.uga.miashs.sempic.entities.SempicAlbum;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Pierre
 */
@Stateless
public class SempicAlbumService {
    @Inject
    private AlbumFacade albumDAO;
    
    @Path("/albums")
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public List<SempicAlbum> listAll(){
        return albumDAO.findAll();
    }

    
    @Path("/albums")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_XML)
    public Response create(SempicAlbum a, @Context UriInfo info) throws SempicModelException{
        long id = albumDAO.create(a);
        URI location = info.getBaseUriBuilder().path(String.valueOf(id)).build();
        return Response.created(location).entity(a).build();
    }
}

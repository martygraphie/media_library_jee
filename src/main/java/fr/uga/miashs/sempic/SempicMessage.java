/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author marty
 */
public class SempicMessage {

    private SempicMessage() {
    }
    private static SempicMessage instance = null;

    public static SempicMessage intance() {
        if (instance == null) {
            instance = new SempicMessage();
        }
        return instance;
    }

    private void getFlash(){
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

    }
    public void successMessage(String text) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text));
        getFlash();
    }
    public void successCreateMessage(String text) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text + " a été crée"));
        getFlash();
    }

    public void successUpdateMessage(String text) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text + " a été mis à jour"));
        getFlash();
    }
    
    public void successDeleteMessage(String text) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text + " a été supprimé"));
        getFlash();
    }
    public void successUpdateAccountMessage() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Votre compte a été mis à jour"));
        getFlash();
    }
    
    public void successDeleteAccountMessage() {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Votre compte a été supprimé"));
        getFlash();
    }
    public void errorMessage(String text) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(text));
        getFlash();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.entities;

import java.nio.file.Paths;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import fr.uga.miashs.sempic.SempicException;
import fr.uga.miashs.sempic.dao.PhotoStorage;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.validation.constraints.NotBlank;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 *
 * @author marty
 */
@Entity
@NamedQueries({
    @NamedQuery(
            name="query.SempicPicture.findAllOwned",
            query="SELECT p FROM SempicPicture p Join p.album a WHERE a.owner=:owner"),
    @NamedQuery(
            name = "query.SempicPicture.findAllOfAlbum",
            query = "SELECT p FROM SempicPicture p WHERE p.album=:album"),
    @NamedQuery(
            name = "query.SempicPicture.findAllPicturesFromIds",
            query = "SELECT p FROM SempicPicture p WHERE p.id IN :picturesIds"),})
public class SempicPicture implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private SempicAlbum album;

    @NotBlank(message = "Un nom de famille doit être donné")
    private String title;

    private String description;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = String.class)
    private List<String> places;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = String.class)
    private List<String> tags;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    private Set<SempicUser> usersTagged;
    
    public SempicPicture() {

    }

    public String getPicturePath() {
        return PhotoStorage.PICTURESTOREWEB.resolve(title).toString();
    }

    public String getThumbnailPath() throws SempicException {
        Path pic = Paths.get(title);

        try {
            PhotoStorage ps = new PhotoStorage(PhotoStorage.URLSTORE, PhotoStorage.URLTHUMBNAILSTORE);
            ps.getThumbnailPath(pic, 120);
            return PhotoStorage.buildAndVerify(PhotoStorage.THUMBNAILSSTOREWEB.resolve(String.valueOf(120)), pic).toString();
        } catch (IOException ex) {
            Logger.getLogger(SempicPicture.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getMediumPath() throws SempicException {
        Path pic = Paths.get(title);
        try {
            PhotoStorage ps = new PhotoStorage(PhotoStorage.URLSTORE, PhotoStorage.URLTHUMBNAILSTORE);
            ps.getThumbnailPath(pic, 400);
            return PhotoStorage.buildAndVerify(PhotoStorage.THUMBNAILSSTOREWEB.resolve(String.valueOf(400)), pic).toString();
        } catch (IOException ex) {
            Logger.getLogger(SempicPicture.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public SempicAlbum getAlbum() {
        return album;
    }

    public void setAlbum(SempicAlbum album) {
        if (album == null) {
            throw new NullPointerException("Album is null");
        }
        this.album = album;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPlaces() {
        if (places == null) {
            places = new ArrayList<>();
        }
        return places;
    }

    public void setPlaces(List<String> places) {
        this.places = places;
    }

    protected void addPlace(String place) {
        if (places == null) {
            places = new ArrayList<>();
        }
        places.add(place);
    }
    
    public List<String> getTags() {
        if (tags == null) {
            tags = new ArrayList<>();
        }
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Set<SempicUser> getUsersTagged() {
        return usersTagged;
    }

    public void setUsersTagged(Set<SempicUser> usersTagged) {
        this.usersTagged = usersTagged;
    }
    
   
    
    protected void addUsersTagged(SempicUser u) {
        if (usersTagged==null) {
            usersTagged = new HashSet<>();
        }
        usersTagged.add(u);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SempicPicture)) {
            return false;
        }
        SempicPicture other = (SempicPicture) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.uga.miashs.sempic.entities.SempicPicture[ id=" + id + " ]";
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.entities;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(uniqueConstraints=@UniqueConstraint(columnNames="title"))
@NamedQueries({
	@NamedQuery(
                name="query.SempicAlbum.findAllOfOwner",
                query="SELECT a FROM SempicAlbum a WHERE a.owner=:owner"),
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
public class SempicAlbum implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    private String title;

    @NotNull
    @ManyToOne
    private SempicUser owner;
    
    @OneToMany(mappedBy = "album", cascade = CascadeType.REMOVE,fetch=FetchType.EAGER)
    private Set<SempicPicture> pictures;
    
    private String description;


    public SempicAlbum() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SempicUser getOwner() {
        return owner;
    }

    public void setOwner(SempicUser owner) {
        this.owner = owner;
    }   
    
    @XmlIDREF
    public Set<SempicPicture> getPictures() {
        if (pictures == null) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(pictures);
    }

    public void setPictures(Set<SempicPicture> pictures) {
        this.pictures = pictures;
    }
    
    protected void addPicture(SempicPicture p) {
        if (pictures == null) {
            pictures = new HashSet<>();
        }
        pictures.add(p);
    }

    @Override
    public String toString() {
        return "Album " + title ;
    }
    
    

    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.backingbeans;

import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.dao.PictureFacade;
import fr.uga.miashs.sempic.entities.SempicPicture;
import fr.uga.miashs.sempic.entities.SempicUser;
import fr.uga.miashs.sempic.rdf.Namespaces;
import fr.uga.miashs.sempic.rdf.PhotoStore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Resource;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author marty
 */
@Named
@ViewScoped
public class SearchController implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LinkedHashMap<String, String> annotationsFilter;

    private String searchWord;

    private String searchPlace;

    private Date searchDate;

    private Long searchUserId;
    
    private Boolean searchOnlyPicturesUser;

    @Inject
    private SessionTools session;

    @Inject
    private PictureFacade pictureDao;
    
    @Inject 
    private UserController userController;

    private List<SempicPicture> pictures;

    private PhotoStore pictureRdf;

    public SearchController() {
        pictureDao = new PictureFacade();
        pictureRdf = new PhotoStore();
    }

    @PostConstruct
    public void init() {
        annotationsFilter = new LinkedHashMap<>();
        annotationsFilter.clear();
        annotationsFilter.put("noplaces", "Photos qui ne sont pas rattachés à un lieu");
        annotationsFilter.put("country", "Photos des villes présentent dans le pays séléctionné");
        annotationsFilter.put("month", "Photos du mois séléctionné");
        annotationsFilter.put("year", "Photos de l'année séléctionné");
        if (session.isAdmin()){
            annotationsFilter.put("usercount", "Photos des utilisateurs ayant au moins deux photos");
        }
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public LinkedHashMap<String, String> getAnnotationsFilter() {
        return annotationsFilter;
    }

    public void setAnnotationsFilter(LinkedHashMap<String, String> annotationsFilter) {
        this.annotationsFilter = annotationsFilter;
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    public String getSearchPlace() {
        return searchPlace;
    }

    public void setSearchPlace(String searchPlace) {
        this.searchPlace = searchPlace;
    }

    public Date getSearchDate() {
        return searchDate;
    }

    public void setSearchDate(Date searchDate) {
        this.searchDate = searchDate;
    }

    public Long getSearchUserId() {
        return searchUserId;
    }

    public void setSearchUserId(Long searchUserId) {
        this.searchUserId = searchUserId;
    }

    public Boolean getSearchOnlyPicturesUser() {
        return searchOnlyPicturesUser;
    }

    public void setSearchOnlyPicturesUser(Boolean searchOnlyPicturesUser) {
        this.searchOnlyPicturesUser = searchOnlyPicturesUser;
    }
    
    public List<SempicUser> completeUserContains(String query) {
        String queryLowerCase = query.toLowerCase();
        List<SempicUser> allUsers = userController.getListUsers();
        return allUsers.stream().filter(t -> t.getFullName().toLowerCase().contains(queryLowerCase)).collect(Collectors.toList());
    }
    
    public void handleSelect(SelectEvent event)  {
        this.searchUserId = Long.parseLong(event.getObject().toString());
    }
  
    public void search() throws SempicModelException {
        List<Resource> listUris = new ArrayList<>();
        long userId = searchOnlyPicturesUser == true ? session.getConnectedUser().getId() : (long)-1;
        String advancedFilter = annotationsFilter.get("entrySet") != null ? annotationsFilter.get("entrySet") : "" ;
        long user = searchUserId ==  null ? (long) -1 : searchUserId;
        listUris = pictureRdf.prepareSearch(searchWord, user, searchPlace, searchDate, advancedFilter, userId);
        if (listUris.size() > 0) {
            List<Long> listIdPictures = new ArrayList<>();
            for (Resource res : listUris) {
                Long idPic = Long.parseLong(StringUtils.removeStart(res.getURI(), Namespaces.photoNS));
                listIdPictures.add(idPic);
            }
            pictures = pictureDao.findAllPicturesFromIds(listIdPictures);
        } else {
            pictures = new ArrayList<>();
        }

    }

    public List<SempicPicture> getPictures() throws SempicModelException {
        if (pictures == null) {
            pictures = session.isAdmin() ? pictureDao.findAll() : pictureDao.findAllOwned(session.getConnectedUser());
        }
        return pictures;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SearchController)) {
            return false;
        }
        SearchController other = (SearchController) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "fr.uga.miashs.sempic.backingbeans.SearchController[ id=" + id + " ]";
    }

}

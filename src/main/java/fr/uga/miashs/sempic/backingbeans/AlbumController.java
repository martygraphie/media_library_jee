/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.backingbeans;


import fr.uga.miashs.sempic.SempicException;
import fr.uga.miashs.sempic.SempicMessage;
import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.SempicModelUniqueException;
import fr.uga.miashs.sempic.dao.AlbumFacade;
import fr.uga.miashs.sempic.dao.SempicUserFacade;
import fr.uga.miashs.sempic.entities.SempicAlbum;
import fr.uga.miashs.sempic.entities.SempicPicture;
import fr.uga.miashs.sempic.entities.SempicUser;
import fr.uga.miashs.sempic.rdf.AlbumStore;
import fr.uga.miashs.sempic.util.AccessRedirect;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;


/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
@Named
@ViewScoped
public class AlbumController implements Serializable {

    private SempicAlbum current;

    @Inject
    private SessionTools session;
    
    @Inject
    private AlbumFacade albumDao;

    @Inject
    private SempicUserFacade userDao;
    
    private AlbumStore albumRdf;

    @Inject
    private PictureController pictureController;

    private DataModel<SempicAlbum> albums;
        
    public AlbumController() {
        userDao = new SempicUserFacade();
        albumDao = new AlbumFacade();
        albumRdf = new AlbumStore();
    }

    @PostConstruct
    public void init() {
        current = new SempicAlbum();
    }
    
    public void accessAlbum(ComponentSystemEvent event) throws IOException{
        if (current == null || (!current.getOwner().equals(session.getConnectedUser()) && !session.isAdmin())) {
          AccessRedirect.intance().redirectAccessDenied();
        }
    }
    
    public List<SempicUser> getUsers() {
        return userDao.findAll();
    }
    
    public void setOwnerId(String id) {
        current.setOwner(userDao.read(Long.valueOf(id)));
    }
    
    public String getOwnerId() {
        if (current.getOwner()==null)
            return "-1";
        return ""+current.getOwner().getId();
    }
    
    
    public void setAlbumId(String id) {
        current = albumDao.read(Long.valueOf(id));
    }

    public String getAlbumId() {
        if (current == null) {
            return "-1";
        }
        return "" + current.getId();
    }    
    public SempicAlbum getCurrent() {
        return current;
    }

    public void setCurrent(SempicAlbum current) {
        this.current = current;
    }
    
    public DataModel<SempicAlbum> getAlbums() throws SempicModelException {
        if (albums == null) {
            if (session.isAdmin()){
                albums = new ListDataModel<>(albumDao.findAll());
            }
            else {
                albums = new ListDataModel<>(albumDao.findAllOfOwner(session.getConnectedUser()));
            }
        }
        return albums;
    }
    
    public Set<SempicPicture> getPictures(){
            return current.getPictures();
    }
    
    public String create() {
        try {
            albumDao.create(current);
            albumRdf.createAlbum(current.getId(), current.getOwner().getId(), current.getTitle(), current.getDescription());
        } catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successCreateMessage(current.toString());
        return "success";
    }
        
    
    public String update() {        
        try {
            albumDao.update(current);
        } 
        catch (SempicModelUniqueException ex) {
            SempicMessage.intance().errorMessage("Un album avec ce nom existe déjà");
            return "failure";
        }
        catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successUpdateMessage(current.toString());
        return "success";
    }
    
    public String delete() throws IOException, SempicException {
        String text;
        Boolean fromList = false;
        try {
            if (current.getTitle() == null){
                fromList = true;
                current = (SempicAlbum) albums.getRowData();
            }
            text = current.toString();
            pictureController.deleteAllPicturesOfAlbum(current);
            current.setPictures(Collections.emptySet());
            albumRdf.deleteAlbum(current.getId());
            albumDao.delete(current);
            if (fromList) {
                albums.setWrappedData(albumDao.findAll());
            }
        } 
        catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successDeleteMessage(text);	
        return "success";
    }
    
    public void deleteAllAlbumsOfUser(SempicUser user) throws IOException, SempicException {
        for (SempicAlbum album : user.getAlbums()) {
           current = album;
           delete();
        }
    }
  
}

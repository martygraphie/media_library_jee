/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.backingbeans;

import fr.uga.miashs.sempic.SempicException;
import fr.uga.miashs.sempic.SempicMessage;
import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.SempicModelUniqueException;
import fr.uga.miashs.sempic.dao.SempicUserFacade;
import fr.uga.miashs.sempic.entities.SempicUser;
import fr.uga.miashs.sempic.rdf.UserStore;
import fr.uga.miashs.sempic.util.AccessRedirect;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotBlank;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
@Named
@ViewScoped
public class UserController implements Serializable {
    
    private SempicUser current;

    @Inject
    private SessionTools session;
    
    @Inject
    private AlbumController albumController;
    
    @Inject
    private SempicUserFacade userDao;
    
    private DataModel users;
    
    private UserStore userRdf;

    public UserController() {
        userDao = new SempicUserFacade();
    }
    
    public void accessUser(ComponentSystemEvent event) throws IOException{
        
        if (current == null || (!current.equals(session.getConnectedUser()) && !session.isAdmin())) {
          AccessRedirect.intance().redirectAccessDenied();
        }
    }
    
    @PostConstruct
    public void init() {
        current=new SempicUser();
        userRdf = new UserStore();
    }

    public String getTitle(String page) {
        String title;
        if (page.equals("update") && session.getConnectedUser().getId() != current.getId()) {
            title = "Modification utilisateur";
        }
        else {
            title = "Mon compte";
        }
        return title;
    }
    
    public List<SempicUser> getListUsers() {
        return userDao.findAll();
    }
    
    public SempicUser getCurrent() {
        return current;
    }

    public void setCurrent(SempicUser current) {
        this.current = current;
    }

    public void setUserId(String id) { 
        current = userDao.read(Long.valueOf(id));
    }
    
    public String getUserId() {
        if (current == null)
            return "-1";
        return ""+ current.getId();
    }
    
    
    public void setPassword(@NotBlank(message="Mot de passe est requis") String password) {
        getCurrent().setPassword(password);
    }
    
    public DataModel<SempicUser> getUsers() {
        if (users == null) {
            users = new ListDataModel<>(userDao.findAll());
        }
        return users;
    }
  
    public String create() {
        try {
            userDao.create(current);
            userRdf.createUser(current.getId(), current.getFirstname(), current.getLastname());
        } 
        catch (SempicModelUniqueException ex) {
            SempicMessage.intance().errorMessage("Un utilisateur avec cette adresse mail existe déjà");
            return "failure";
        }
        catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successCreateMessage(current.getEmail());
        return "success";
    }
    
    public String update() {        
        try {
            userDao.update(current);
        } 
        catch (SempicModelUniqueException ex) {
            SempicMessage.intance().errorMessage("Un utilisateur avec cette adresse mail existe déjà");
            return "failure";
        }
        catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        if (session.isAdmin()){
            SempicMessage.intance().successUpdateMessage(current.getEmail());
            return "success_admin";
        }
        else {
            SempicMessage.intance().successUpdateAccountMessage();
            return "success_user"; 
        }

    }
    
    public String delete() throws IOException, SempicException {
        String text;
        Boolean fromList = false;
        try {
            if (current.getEmail() == null){
                fromList = true;
                current = (SempicUser) users.getRowData();
            }
            text = current.getEmail();
            albumController.deleteAllAlbumsOfUser(current);
            userRdf.deleteUser(current.getId());
            userDao.delete(current);
            if (fromList) {
                users.setWrappedData(userDao.findAll());
            }
        } 
        catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        if (session.isAdmin()){
            SempicMessage.intance().successDeleteMessage(current.getEmail());
            return "success_admin";
        }
        else {
            session.logout();
            SempicMessage.intance().successDeleteAccountMessage();
            return "success_user"; 
        }
    }
}

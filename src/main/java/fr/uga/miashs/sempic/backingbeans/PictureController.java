/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.backingbeans;

import fr.uga.miashs.sempic.SempicException;
import fr.uga.miashs.sempic.SempicMessage;
import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.SempicModelUniqueException;
import fr.uga.miashs.sempic.dao.AlbumFacade;
import fr.uga.miashs.sempic.dao.PhotoStorage;
import fr.uga.miashs.sempic.dao.PictureFacade;
import fr.uga.miashs.sempic.dao.SempicUserFacade;
import fr.uga.miashs.sempic.entities.SempicAlbum;
import fr.uga.miashs.sempic.entities.SempicPicture;
import fr.uga.miashs.sempic.model.rdf.SempicOnto;
import fr.uga.miashs.sempic.rdf.Namespaces;
import fr.uga.miashs.sempic.rdf.PhotoStore;
import fr.uga.miashs.sempic.util.AccessRedirect;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ComponentSystemEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.shaded.commons.io.FilenameUtils;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */
@Named
@RequestScoped
public class PictureController implements Serializable {

    private SempicPicture current;

    @Inject
    private SessionTools session;
    
    @Inject
    private PictureFacade pictureDao;

    @Inject
    private AlbumFacade albumDao;

    @Inject
    private PhotoStorage pictureStorage;

    private PhotoStore pictureRdf;
    
    private DataModel<SempicPicture> pictures;

    private UploadedFile file;
    
    @Inject
    private SempicUserFacade userDao;
    
    public PictureController() {

    }

    @PostConstruct
    public void init() {
        current = new SempicPicture();
        pictureRdf = new PhotoStore();
    }
    public void accessPicture(ComponentSystemEvent event) throws IOException{
        if (current.getAlbum() == null || (!current.getAlbum().getOwner().equals(session.getConnectedUser()) && !session.isAdmin())) {
          AccessRedirect.intance().redirectAccessDenied();
        }
    }
    public void setAlbumId(String id) {
        current.setAlbum(albumDao.read(Long.valueOf(id)));        
    }

    public String getAlbumId() {
        if (current.getAlbum() == null) {
            return "-1";
        }
        return "" + current.getAlbum().getId();
    }

    public void setPictureId(String id) {
        current = pictureDao.read(Long.valueOf(id));
    }

    public String getPictureId() {
        if (current == null) {
            return "-1";
        }
        return "" + current.getId();
    }

    public SempicPicture getCurrent() {
        return current;
    }

    public void setCurrent(SempicPicture current) {
        this.current = current;
    }
    
    public DataModel<SempicPicture> getPicturesOfAlbum(SempicAlbum album) throws SempicModelException {
        if (pictures == null) {
            pictures = new ListDataModel<>(pictureDao.findAllOfAlbum(album));
        }
        return pictures;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void fileUploadListener(FileUploadEvent e) {
        this.file = e.getFile();
    }

    public void uploadFile() throws SempicException {
        try (InputStream input = file.getInputstream()) {
            Date date= new Date();
            String name = session.getConnectedUser().getId() + "-" + FilenameUtils.getBaseName(file.getFileName()) + "-" + date.getTime() ;
            pictureStorage = new PhotoStorage(PhotoStorage.URLSTORE, PhotoStorage.URLTHUMBNAILSTORE);
            pictureStorage.savePicture(PhotoStorage.URLSTORE.resolve(name), input);
            current.setTitle(name);
        } catch (IOException e) {
        }
    }   

    public String create() throws SempicException, ParseException {
        try {
            uploadFile();
            pictureDao.create(current);
            createRDF();
        } catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successCreateMessage(current.getTitle());
        return "success";

    }
    
    public void createRDF() throws SempicException, ParseException {
        pictureRdf.createPhoto(current.getId(), current.getAlbum().getId(), current.getAlbum().getOwner().getId(), current.getTitle(), current.getDescription());
        initAnnotations();
    }
    public void updateRDF() throws SempicException, ParseException {
        Resource picture = ResourceFactory.createResource(Namespaces.getPhotoUri(current.getId()));
        pictureRdf.deleteAnnotation(picture, SempicOnto.represente);
        pictureRdf.deleteAnnotation(picture, SempicOnto.takenWhere);
        pictureRdf.deleteAnnotation(picture, SempicOnto.takenDate);
        initAnnotations();
    }
        
    public void initAnnotations() throws ParseException {
        if (current.getDate() != null){
            pictureRdf.addAnnotationWhen(current.getId(), current.getDate());
        }
        if (!current.getPlaces().isEmpty()){
            current.getPlaces().forEach((place) -> {
                pictureRdf.addAnnotationWhere(current.getId(), place);
            });
        }
        if (!current.getTags().isEmpty()){
            current.getTags().forEach((tag) -> {
                pictureRdf.addAnnotationWhat(current.getId(), tag);
            });
        }
        if (!current.getUsersTagged().isEmpty()){
            current.getUsersTagged().forEach((user) -> {
                pictureRdf.addAnnotationPerson(current.getId(), user.getId());
            });
        }
    }

    public String update() throws SempicException, ParseException {
        try {
            pictureDao.update(current);
            updateRDF();
        } catch (SempicModelUniqueException ex) {
            SempicMessage.intance().errorMessage("La photo existe deja");
            return "failure";
        } catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successUpdateMessage(current.getTitle());
        return "success";
    }

    public String delete() throws IOException, SempicException {
        String text;
        try {
            text = current.getTitle();
            pictureRdf.deletePhoto(current.getId());
            pictureStorage = new PhotoStorage(PhotoStorage.URLSTORE, PhotoStorage.URLTHUMBNAILSTORE);
            pictureStorage.deletePicture(Paths.get(current.getTitle()));
            pictureDao.delete(current);
        } catch (SempicModelException ex) {
            SempicMessage.intance().errorMessage(ex.getMessage());
            return "failure";
        }
        SempicMessage.intance().successDeleteMessage(text);
        return "success";
    }
    
    public void deleteAllPicturesOfAlbum(SempicAlbum album) throws IOException, SempicException {
        for (SempicPicture picture : album.getPictures()) {
           current = picture;
           delete();
        }
    }
   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.util;

import java.io.IOException;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;

/**
 *
 * @author marty
 */
public class AccessRedirect {

    private AccessRedirect() {}
    
    private static final AccessRedirect instance = new AccessRedirect();

    public static AccessRedirect intance() {
        return instance;
    }
    
    public void redirectAccessDenied() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) context.getApplication().getNavigationHandler();
        nav.performNavigation("error-403");
    }
    
}

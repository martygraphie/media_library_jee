/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.dao;

import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.entities.SempicAlbum;
import fr.uga.miashs.sempic.entities.SempicUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */

@Stateless
public class AlbumFacade extends AbstractJpaFacade<Long,SempicAlbum> {

    
    public AlbumFacade() {
        super(SempicAlbum.class);
    }
    
    public List<SempicAlbum> findAllOfOwner(SempicUser user) throws SempicModelException {
            Query q = getEntityManager().createNamedQuery("query.SempicAlbum.findAllOfOwner");
            q.setParameter("owner", getEntityManager().merge(user));
            return q.getResultList();
    }

    @Override
    public Long create(SempicAlbum album) throws SempicModelException {
        return super.create(album);
    }
 
    public void addMember(long albumId, long userId) {
        Query q = getEntityManager().createNativeQuery("INSERT INTO SEMPICALBUM_SEMPICUSER(MEMBERSALBUM_ID,ACCESSOFALBUM_ID) VALUES (?1,?2)");
        q.setParameter(1,userId);
        q.setParameter(2,albumId);
        q.executeUpdate();
    }
    
    public void deleteMember(long albumId, long userId) {
        Query q = getEntityManager().createNativeQuery("DELETE FROM SEMPICALBUM_SEMPICUSER WHERE MEMBERSALBUM_ID=?1 AND ACCESSOFALBUM_ID=?2");
        q.setParameter(1,userId);
        q.setParameter(2,albumId);
        q.executeUpdate();
    }
    
}

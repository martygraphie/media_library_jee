/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.uga.miashs.sempic.dao;

import fr.uga.miashs.sempic.SempicModelException;
import fr.uga.miashs.sempic.entities.SempicAlbum;
import fr.uga.miashs.sempic.entities.SempicPicture;
import fr.uga.miashs.sempic.entities.SempicUser;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;


/**
 *
 * @author Jerome David <jerome.david@univ-grenoble-alpes.fr>
 */

@Stateless
public class PictureFacade extends AbstractJpaFacade<Long,SempicPicture> {

    
    public PictureFacade() {
        super(SempicPicture.class);
    }
    
    @Override
    public Long create(SempicPicture picture) throws SempicModelException {
        return super.create(picture);
    }

    public List<SempicPicture> findAllOwned(SempicUser user) throws SempicModelException {
        Query q = getEntityManager().createNamedQuery("query.SempicPicture.findAllOwned");
        q.setParameter("owner", getEntityManager().merge(user));
        return q.getResultList();
    }    
    
    public List<SempicPicture> findAllOfAlbum(SempicAlbum album) throws SempicModelException {
        Query q = getEntityManager().createNamedQuery("query.SempicPicture.findAllOfAlbum");
        q.setParameter("album", getEntityManager().merge(album));
        return q.getResultList();
    }
    
    public List<SempicPicture> findAllPicturesFromIds(List<Long> picturesIds) throws SempicModelException {
    Query q = getEntityManager().createNamedQuery("query.SempicPicture.findAllPicturesFromIds");
    q.setParameter("picturesIds", picturesIds);
    return q.getResultList();
    }   
}

# MEDIA LIBRARY JEE

## ABSTRACT 

The goal of the project is to create a photo album management and annotation application 
annotation application using semantic web technologies.
The project must use the following technologies: the front-end in JSF, 
persistence in JPA, web services in Jax-RS. 


## CONTEXT

The application must be multi-user. A user can create an account and connect. 
account and log in.
A logged-in user can create albums and add photos to them.
A user can delete photos and albums (in this case, all photos in the album must be deleted). 
photos in the album must be deleted).

The user can view the list of his albums and also the photos of the albums.
An admin interface allows to list the accounts and to delete an 
account.

The user will be able to annotate the photos of his albums (i.e. those he has created).
The possible annotations on a photo are: the creator, when the photo was taken, where the 
taken, where the photo was taken, who is in the photo and what is in the 
photo. Places, people, and objects will be entities in their own right.

This will allow you to create relationships between them: a place can be 
contained in another place, an object (for example a monument) is located in a place, and finally
a place, and finally you can create groups of people.

A multi-criteria search interface (who, what, when) will be proposed.

## PREREQUISITES

Download and install the following servers: 
  * Tome EE: https://www.apache.org/dyn/closer.cgi/tomee/tomee-8.0.0/apache-tomee-8.0.0-plume.tar.gz
  * Apache Jena Fuseki : 3.13.0

## INSTALLATION
    1. Download the SempicJPA & SempicRDF projects and place them at the same level 
       level as the Apache Jena Fuseki server.
    2. Open the SempicRDF project and build it. 
    3. Open the SempicJPA project, add the SempicRDF project as a dependency and build 
       dependency and build it.
    4. get the provided database and add it in Apache Tome EE
    5. Recompile the SempicJPA program and run it.
    
## LOGIN INFORMATION
  * login : admin@miashs.fr password : admin

## ACHIEVEMENT

### BACK-END

#### GENERAL

The INSTAMIASH project implements all the features requested. 
It is possible to add, modify, delete a user. Groups and 
albums can be attached to it. 

An album can contain one or more photos. It is possible to assign
a place, a date, to identify users on the photo, and to add
tags to it.

When a user is deleted, the entities attached to him are also deleted. 
also deleted.

#### MENUS

The menu is specific to each role. As long as the user is not connected 
he cannot access the different pages of the application.

Administrator menu :  
  * Home 
  * Users 
  * Albums
  * Profile 
  * Logout

User menu:  
  * Home 
  * Albums
  * Profile 
  * Disconnect

#### SEARCH 

A search engine has been implemented on the home page, it allows to 
search for photos added by users. using different filters.
the photos rendered by default:
  * the user role accesses the list of his photos.
  * The administrator role has access to all the photos of the application.

However, from the moment an administrator performs a search, 
he will only have access to his photos.
The search engine offers 5 functional filters:
  * User: allows you to filter the photos where
      a user of the application has been identified.

  * Location: allows you to search for photos tagged in a location. 

  * Keyword: allows you to filter photos containing the keyword entered.

  * Date: allows you to search for photos on a specific date.

  * Advanced filter: the advanced filter allows you to combine the above mentioned filters with 
        with other conditions, the user can then :
    - display the photos that are not attached to a place (the field 
        place field must be empty)
    - display the photos present in the selected country (a country name must be given) 
        country must be given)
    - display the photos of the selected month (a date must be filled)
    - display the photos of the selected year (a date must be filled in)

The administrator also has the possibility to search only in his 
or in all the photos of the users. A filter is available 
only for the administrator. He can display only the photos of a user who has at least 
user who has at least two photos.

#### SECURITY

An access management has also been created on the site. A user with 
the role USER can only see, modify and delete his content.
 
If he tries to access the information of other users he will be redirected 
to a 403 error page. While the administrator role can access, modify and delete all 
contents, modify and delete them.

In case a user tries to access a page that does not exist, 
he will be redirected to a 404 error page.

#### JENA 

The data of the Apache Jena Fuseki server are linked to entities. When a 
user creates, adds or deletes entities, this is also reflected on the 
reflected on the data of the Apache Jena Fuseki server 
To perform all the searches, SPARQL technology has been used.


### FRONT-END

The Java Faces templates have been split into components. 
In order to improve the experience and the user interface the following libraries 
have been used:
  * Primefaces : https://www.primefaces.org/showcase/getstarted.xhtml
  * Materialize : https://materializecss.com/getting-started.html


## REMARKS 

The main problem is that we worked on two operating systems so we chose to put our files in the resources folder. They
should not be stored there because it is difficult to have a stable control.
This choice implies a bug in the application, involving the refreshment of the page when a photo is added 

/!\ For the photo addition to work, think of modifying the variables

public static final Path URLSTORE = Paths.get("/{your_absolute_path}/resources/images/original");
public static final Path URLTHUMBNAILSTORE = Paths.get("{your_absolute_path}/resources/images/thumbnails");

from file src/main/java/en/uga/miashs/sempic/dao/PhotoStorage.java

Project created by Marty Marc-Antoine
